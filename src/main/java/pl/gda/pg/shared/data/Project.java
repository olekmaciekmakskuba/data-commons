package pl.gda.pg.shared.data;

import java.util.List;

public class Project {

    private String name;
    private List<File> projectFiles;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<File> getProjectFiles() {
        return projectFiles;
    }

    public void setProjectFiles(List<File> projectFiles) {
        this.projectFiles = projectFiles;
    }
}