package pl.gda.pg.shared.data;

public enum Role {
    ADMIN,
    STUDENT,
    TEACHER
}
