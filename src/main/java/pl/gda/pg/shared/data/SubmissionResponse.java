package pl.gda.pg.shared.data;

import java.util.List;

public class SubmissionResponse {

	private int id;

	private List<SubmissionDetail> details;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<SubmissionDetail> getDetails() {
		return details;
	}

	public void setDetails(List<SubmissionDetail> details) {
		this.details = details;
	}
}
