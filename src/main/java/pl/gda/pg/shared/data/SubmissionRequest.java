package pl.gda.pg.shared.data;

import java.util.List;

public class SubmissionRequest {

	private int id;

	private List<File> files;

	public List<File> getFiles() {
		return files;
	}

	public void setFiles(List<File> files) {
		this.files = files;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
