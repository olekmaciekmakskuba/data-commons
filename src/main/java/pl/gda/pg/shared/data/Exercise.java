package pl.gda.pg.shared.data;

import java.util.List;

public class Exercise {

    private String name;
    private String task;
    private Language language;
    private List<File> template;

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public List<File> getTemplate() {
        return template;
    }

    public void setTemplate(List<File> template) {
        this.template = template;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
