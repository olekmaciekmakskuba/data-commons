package pl.gda.pg.shared.services;

public enum VertxServices {
    COMPILER("compiler.gradle.compile"),
    DATABASE_SUBMISSION_FIND("database.submission.find"),
    DATABASE_SUBMISSION_UPDATE("database.submission.update"),
    DATABASE_SUBMISSION_SAVE("database.submission.save");

    private final String address;

    VertxServices(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

}
